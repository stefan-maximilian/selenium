package de.teekaa.selenium;

import de.teekaa.selenium.driverfactories.ChromeReusableRemoteWebDriver;
import org.fluentlenium.core.FluentPage;
import org.openqa.selenium.WebDriver;

import java.net.MalformedURLException;

public class AmazonPage extends FluentPage {
    public AmazonPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public String getUrl() {
        return "https://www.amazon.de";
    }

    public static void main(String[] args) throws MalformedURLException {
//        WebDriver driver = JBrowserDriverFactory.create();
        WebDriver driver = ChromeReusableRemoteWebDriver.createChromeDriver();


        AmazonPage page = new AmazonPage(driver);
        page.go();
        page.step1ClickAnmelden();
        page.step2Anmelden();
        page.takeScreenShot("d:/tee.png");
    }

    private void step1ClickAnmelden() {
        findFirst("#nav-link-yourAccount").click();
    }

    private void step2Anmelden() {
        findFirst("#ap_email").text("qqq");
//        findFirst("#ap_password").text("qqq");
//        findFirst("#signInSubmit").click();
    }
}
