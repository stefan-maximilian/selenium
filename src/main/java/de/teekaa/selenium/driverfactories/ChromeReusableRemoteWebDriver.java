package de.teekaa.selenium.driverfactories;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.remote.*;
import org.openqa.selenium.remote.http.HttpClient;
import org.openqa.selenium.remote.internal.ApacheHttpClient;
import org.openqa.selenium.remote.internal.HttpClientFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

public class ChromeReusableRemoteWebDriver extends RemoteWebDriver {
    private static Logger log = LoggerFactory.getLogger(ChromeReusableRemoteWebDriver.class);

    public ChromeReusableRemoteWebDriver(HttpCommandExecutor executor, Capabilities desiredCapabilities, Capabilities requiredCapabilities) {
        super(executor, desiredCapabilities, requiredCapabilities);
        try {
            Field field =RemoteWebDriver.class.getDeclaredField("capabilities");
            field.setAccessible(true);
            field.set(this, desiredCapabilities);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setSessionId(String opaqueKey) {
        super.setSessionId(opaqueKey);
    }

    @Override
    protected void startSession(Capabilities desiredCapabilities, Capabilities requiredCapabilities) {
        String sid = getPreviousSessionIdFromSomeStorage();
        if (sid != null) {
            setSessionId(sid);
            try {
                getCurrentUrl();
            } catch (WebDriverException e) {
                // Session nicht gültig
                sid = null;
            }
        }

        if (sid == null) {
            super.startSession(desiredCapabilities, requiredCapabilities);
            saveSessionIdToSomeStorage();
        }
    }

    private void saveSessionIdToSomeStorage() {
        try {
            File file = getChromeSessionFile();
            Files.write(file.toPath(), getSessionId().toString().getBytes(), StandardOpenOption.CREATE);
        } catch (IOException e) {
            throw new RuntimeException("Session-ID konnte nicht gespeichert werden", e);
        }
    }

    private File getChromeSessionFile() {
        URL chromeSession = ChromeReusableRemoteWebDriver.class.getResource("/chrome/chromesession.txt");
        return new File(chromeSession.getFile());
    }

    private String getPreviousSessionIdFromSomeStorage() {
        try {
            File chromeSessionFile = getChromeSessionFile();
            byte[] bytes = Files.readAllBytes(chromeSessionFile.toPath());
            return new String(bytes);
        } catch (IOException e) {
            throw new RuntimeException("Session-ID konnte nicht eingelesen werden", e);
        }
    }

    public static WebDriver createChromeDriver() throws MalformedURLException {
        try {
            URL chromeDriverUrl = new URL("http://localhost:9515");
            HttpURLConnection urlConnection = (HttpURLConnection) chromeDriverUrl.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setConnectTimeout(3000);
            urlConnection.setReadTimeout(3000);
            urlConnection.getInputStream();
        } catch (ConnectException e) {
            startDriver();
        } catch (ProtocolException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            // z.B. FileNotFound --> Chromedriver bereits gestartet
        }

        HttpClient.Factory factory = new ApacheHttpClient.Factory(new HttpClientFactory(25 * 1000, 25 * 1000));
        HttpCommandExecutor executor = new HttpCommandExecutor(Collections.<String, CommandInfo> emptyMap(), new URL("http://localhost:9515"), factory);

        DesiredCapabilities desiredCapabilities = DesiredCapabilities.chrome();
        desiredCapabilities.setJavascriptEnabled(true);

        DesiredCapabilities requiredCapabilities = DesiredCapabilities.chrome();
        requiredCapabilities.setJavascriptEnabled(true);

        return createChromeDriver(executor, desiredCapabilities, requiredCapabilities);
    }

    private static void startDriver() {
        File chromeDriverFile = new File("../driver/chromedriver.exe");
        String chromeDriverFilename = chromeDriverFile.getAbsolutePath();
        log.info("ChromeDriver wird gestartet: " + chromeDriverFilename);

        ProcessBuilder pb = new ProcessBuilder(chromeDriverFilename);
        File parentFolder = chromeDriverFile.getParentFile();
        log.info("ChromeDriver-Directory: " + parentFolder);
        pb.directory(parentFolder);
        pb.redirectErrorStream(true);
        try {
            pb.start();
            log.info("ChromeDriver gestartet");
        } catch (IOException e1) {
            throw new RuntimeException(e1);
        }
    }

    private static WebDriver createChromeDriver(HttpCommandExecutor executor, DesiredCapabilities desiredCapabilities, DesiredCapabilities requiredCapabilities) {
        ChromeReusableRemoteWebDriver driver = new ChromeReusableRemoteWebDriver(executor, desiredCapabilities, requiredCapabilities);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        return driver;
    }
}
