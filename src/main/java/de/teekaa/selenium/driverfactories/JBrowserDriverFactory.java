package de.teekaa.selenium.driverfactories;

import com.machinepublishers.jbrowserdriver.JBrowserDriver;
import com.machinepublishers.jbrowserdriver.Settings;
import com.machinepublishers.jbrowserdriver.Timezone;
import com.machinepublishers.jbrowserdriver.UserAgent;
import org.openqa.selenium.WebDriver;

public class JBrowserDriverFactory {
    public static WebDriver create() {
        JBrowserDriver driver = new JBrowserDriver(Settings.builder().userAgent(UserAgent.CHROME).
                blockAds(true).ajaxResourceTimeout(10 * 1000).
                timezone(Timezone.EUROPE_BERLIN).build());
        return driver;
    }
}
